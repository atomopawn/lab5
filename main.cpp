#include "SSDL.h"

#include <unistd.h>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>

#include "player.h"
#include "ship.h"
#include "game.h"

using namespace std;

//Number of ships the invasion fleet starts with

int main(int argc, char* argv[])
{
	// Initialize random number generator
	srandom(7); 
	
	//Initialize SSDL
	SSDL_SetWindowSize(1024, 768);

	//Create an object representing the player's ship
        SSDL_Image playerImage = SSDL_LoadImage("Ship.png");
	Player p(playerImage);

	//Create ships representing the invasion fleet
        vector<Ship> ships;

        SSDL_Image shipImage = SSDL_LoadImage("Ship2.png");
	for (int i=0; i < INITIAL_SHIPS; ++i) {
		Ship s(shipImage);
                ships.push_back(s);
        }
	
	int counter = 0; //How many "time units" have elapsed since the start of the game?
	bool alive = true; // Has the invasion fleet caught the player or not?

	//Main Game Loop
	while (!SSDL_IsQuitMessage() && alive) {

		//Draw the background objects (including the "score")
                SSDL_RenderClear();
		SSDL_RenderTextCentered(ships.size(), 512,10);

		//Move and Draw the invasion fleet
		for (auto& s: ships) {
                        s.update();
                        s.draw();
                }

		//Move the player
		if (SSDL_IsKeyPressed(SDLK_UP)) {
                        p.up();
                }

                if (SSDL_IsKeyPressed(SDLK_DOWN)) {
                        p.down();
                }

                p.draw();

		//Check for collisions
		for (int i = 0; i < ships.size(); ++i) {
                        if (p.collides_with(ships[i]))
                        {
                                sout << "Gave over!  You survived a fleet of " << ships.size() << " ships!" << endl;
                                if (ships.size() >= 50) {
					sout << "Well done!  Your ship was destroyed -- but not before you came within transmission range of Earth and" << endl << "gave your warning.  The world is saved!" << endl;	
				} else if (ships.size() > 20) {
                                        sout << "Congratulations!" << endl;
                                }
                                else {
                                        sout << "You can do better." << endl;
                                }
                                alive = false;
				break; 
                        }
                }

                //Add a new ship every 100 updates
                ++counter;
                if (counter % 100 == 0) {
                        ships.push_back(Ship(shipImage));
                }

                SSDL_Delay(50);
        }

	SSDL_WaitKey();
}
